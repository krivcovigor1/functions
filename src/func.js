const getSum = (str1, str2) => {
  if (typeof str1 === 'string' && typeof str2 === 'string') {
    if (str1 === '') { str1 = 0 + '' }
    if (str2 === '') { str2 = 0 + '' }
    if (str1.match(/^\d+$/) && str2.match(/^\d+$/)) {
      let result = (parseInt(str1) + parseInt(str2));
      return result + '';
    } else { return false }
  } else { return false }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numOfPosts = 0;
  let numOfComments = 0;
  for (let elem of listOfPosts) {
    if (elem.author === authorName) {
      numOfPosts++;
    }
    if (elem.comments) {
      for (let el of elem.comments) {
        if (el.author === authorName) {
          numOfComments++
        }
      }
    }
  }
  return `Post:${numOfPosts},comments:${numOfComments}`;
};

const tickets = (people) => {
  let cashToChange = 0;
    let arrResults = [];
    let oneTicket = 25;
    for(let el in people) {
        if(typeof people[el] === 'string') {people[el] = parseInt(people[el])}
        if(people[el] === oneTicket) {
            arrResults.push(true);
            cashToChange += people[el];
        }else if(people[el]>25) {
            if(people[el]-oneTicket <= cashToChange) {
                arrResults.push(true);
                cashToChange += people[el]-oneTicket;
            }else {
                arrResults.push(false);
            }
        }
    }
    if(arrResults.every((el) => el === true)) {
        return 'YES';
    }else if(!arrResults.every((el) => el === true)) {
        return 'NO';
    }
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
